/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package fizzBuzz;

import java.util.List;

public class App {

    public static void main(String[] args) {
        List<Integer> numbersList = NumberGenerator.generateNumbers(1 , 100);
        for ( Integer fizzBuzzResult : numbersList ) {
        	System.out.println(FizzBuzzPrinter.fizzBuzzPrinter(fizzBuzzResult));
        }
    }

}
